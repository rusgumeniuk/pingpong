﻿namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IEventBusConfiguration
    {
        string Username { get; }
        string Password { get; }
        string VirtualHost { get; }
        string HostName { get; }
        int Port { get; }
        string Uri { get; }
    }
}
