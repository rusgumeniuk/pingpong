﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IEventBus : IDisposable
    {
        void SendMessageToQueue(string message);
        void ListenQueue();
        void ProcessEvent(string key, string message);
    }
}
