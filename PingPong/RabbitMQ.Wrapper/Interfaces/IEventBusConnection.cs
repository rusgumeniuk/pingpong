﻿using RabbitMQ.Client;

using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IEventBusConnection : IDisposable
    {
        bool IsConnected { get; }
        bool TryConnect();
        IModel CreateModel();
    }
}