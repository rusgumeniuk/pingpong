﻿using Microsoft.Extensions.Configuration;

using RabbitMQ.Wrapper.Interfaces;

using System;

namespace RabbitMQ.Wrapper.Models
{
    public class RabbitMqConfiguration : IEventBusConfiguration
    {
        public string Username { get; }
        public string Password { get; }
        public string VirtualHost { get; }
        public string HostName { get; }
        public int Port { get; }
        public string Uri { get; }

        public RabbitMqConfiguration(IConfiguration configuration)
        {
            var rabbitMqConfigurationSection = configuration.GetSection("RabbitMQ")
                          ?? throw new ArgumentNullException("RabbitMQ section");
            Username = rabbitMqConfigurationSection["username"];
            Password = rabbitMqConfigurationSection["password"];
            HostName = rabbitMqConfigurationSection["hostName"];
            VirtualHost = rabbitMqConfigurationSection["virtualHost"];
            Port = int.Parse(rabbitMqConfigurationSection["port"] ?? "0");
            Uri = rabbitMqConfigurationSection["uri"];
        }
    }
}
