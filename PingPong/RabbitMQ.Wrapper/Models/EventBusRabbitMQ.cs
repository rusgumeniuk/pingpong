﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;

using System;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper.Models
{
    public class EventBusRabbitMq : IEventBus
    {
        private readonly IEventBusConnection _connection;

        private readonly string _exchangeName;

        private readonly string _consumerQueueName;

        private readonly string _producerKey;
        private readonly string _consumerKey;

        private IModel _consumerChannel;

        public EventBusRabbitMq(
            string exchangeName,
            string consumerQueueName,
            string consumerKey,
            string producerKey,
            IEventBusConnection connection)
        {
            _exchangeName = exchangeName;

            _consumerQueueName = consumerQueueName;

            _producerKey = producerKey;
            _consumerKey = consumerKey;

            _connection = connection;
            _consumerChannel = CreateConsumerChannel();
        }

        #region Producer

        public void SendMessageToQueue(string message)
        {
            if (!_connection.IsConnected)
            {
                _connection.TryConnect();
            }

            Debug.WriteLine($"[Info] Creating channel to send next message: {message}");

            using (var channel = _connection.CreateModel())
            {
                channel.ExchangeDeclare(_exchangeName, ExchangeType.Direct);

                var body = Encoding.UTF8.GetBytes(message);
                var props = channel.CreateBasicProperties();
                channel.BasicPublish(
                    _exchangeName,
                    _producerKey,
                    props,
                    body);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"[x] Sent: {message} at {DateTime.Now}");
            }
        }
        #endregion

        #region Consumer
        public void ListenQueue()
        {
            if (_consumerChannel == null)
            {
                Debug.WriteLine("[Error] ListenQueue can't call on _consumerChannel == null");
                _consumerChannel = CreateConsumerChannel();
            }

            var consumer = new EventingBasicConsumer(_consumerChannel);

            consumer.Received += Consumer_Received;

            _consumerChannel.BasicConsume(
                _consumerQueueName,
                false,
                consumer);
            while (true)
            {
                Thread.Sleep(50);
            }
        }

        private void Consumer_Received(object sender, BasicDeliverEventArgs eventArgs)
        {
            var key = eventArgs.RoutingKey;
            var message = Encoding.UTF8.GetString(eventArgs.Body.ToArray());

            try
            {
                ProcessEvent(key, message);
                Thread.Sleep(2500);
                SendMessageToQueue(_consumerKey);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[Error] {ex.Message}");
            }

            _consumerChannel.BasicAck(eventArgs.DeliveryTag, false);
        }

        public void ProcessEvent(string key, string message)
        {
            Debug.WriteLine($"[Info] Process key: {key} with message: {message}");

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine($"[x] Received: {message} at {DateTime.Now}");
        }
        #endregion

        private IModel CreateConsumerChannel()
        {
            if (!_connection.IsConnected)
            {
                _connection.TryConnect();
            }

            var channel = _connection.CreateModel();

            channel.ExchangeDeclare(_exchangeName, ExchangeType.Direct);
            channel.QueueDeclare(_consumerQueueName,
                true,
                false,
                false,
                null);

            channel.QueueBind(queue: _consumerQueueName,
                exchange: _exchangeName,
                routingKey: _consumerKey);

            channel.CallbackException += (sender, ea) =>
            {
                Debug.WriteLine(ea.Exception);

                _consumerChannel = CreateConsumerChannel();
                ListenQueue();
            };
            return channel;
        }

        public void Dispose()
        {
            _consumerChannel?.Dispose();
        }
    }
}
