﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;

using System;
using System.Diagnostics;

namespace RabbitMQ.Wrapper.Models
{
    public class DefaultConnectionRabbitMq : IEventBusConnection
    {
        private readonly IConnectionFactory _connectionFactory;
        private IConnection _connection;
        private bool _disposed;

        private readonly object _syncRoot = new object();

        public DefaultConnectionRabbitMq(IEventBusConfiguration configuration)
        {
            _connectionFactory = new ConnectionFactory()
            {
                UserName = configuration.Username,
                Password = configuration.Password,
                HostName = configuration.HostName,
                VirtualHost = configuration.VirtualHost,
                Port = configuration.Port
            };
        }

        public bool IsConnected => _connection?.IsOpen == true && !_disposed;

        public bool TryConnect()
        {
            Debug.WriteLine("[Info] RabbitMQ Client is trying to connect");

            lock (_syncRoot)
            {
                _connection = _connectionFactory.CreateConnection();

                if (IsConnected)
                {
                    _connection.ConnectionShutdown += OnConnectionShutdown;
                    _connection.CallbackException += OnCallbackException;
                    _connection.ConnectionBlocked += OnConnectionBlocked;

                    Debug.WriteLine("[Info] RabbitMQ Client acquired a persistent connection to '{HostName}' and is subscribed to failure events");

                    return true;
                }
                else
                {
                    Debug.WriteLine("[Error] FATAL ERROR: RabbitMQ connections could not be created and opened");
                    return false;
                }
            }
        }

        private void OnConnectionBlocked(object sender, ConnectionBlockedEventArgs e)
        {
            if (_disposed) return;

            Debug.WriteLine("[Error] A RabbitMQ connection is shutdown. Trying to re-connect...");

            TryConnect();
        }

        private void OnCallbackException(object sender, CallbackExceptionEventArgs e)
        {
            if (_disposed) return;

            Debug.WriteLine("[Error] A RabbitMQ connection throw exception. Trying to re-connect...");

            TryConnect();
        }

        private void OnConnectionShutdown(object sender, ShutdownEventArgs reason)
        {
            if (_disposed) return;

            Debug.WriteLine("[Error] A RabbitMQ connection is on shutdown. Trying to re-connect...");

            TryConnect();
        }

        public IModel CreateModel()
        {
            if (!IsConnected)
            {
                throw new InvalidOperationException("[Error] No RabbitMQ connections are available to perform this action");
            }

            return _connection.CreateModel();
        }

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;

            try
            {
                _connection.Dispose();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[Error] Exception thrown when dispose connection : {ex.Message}");
            }
        }
    }
}
