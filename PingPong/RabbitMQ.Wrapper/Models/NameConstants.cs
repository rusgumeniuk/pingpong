﻿namespace RabbitMQ.Wrapper.Models
{
    public static class NameConstants
    {
        public const string PingPongExchange = "ping_pong_exchange";

        public const string PingQueue = "ping_queue";
        public const string PongQueue = "pong_queue";

        public const string PingKey = "ping";
        public const string PongKey = "pong";
    }
}
