﻿
using Microsoft.Extensions.Configuration;

using RabbitMQ.Wrapper.Models;

using System;

namespace Pinger
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WindowWidth = 50;
            Console.WriteLine("Pinger started!");

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            using var eventBus = new EventBusRabbitMq(
                NameConstants.PingPongExchange,
                NameConstants.PingQueue,
                NameConstants.PingKey,
                NameConstants.PongKey,
                new DefaultConnectionRabbitMq(new RabbitMqConfiguration(config)));

            eventBus.SendMessageToQueue(NameConstants.PingKey);
            eventBus.ListenQueue();
        }
    }
}
