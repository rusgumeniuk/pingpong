﻿
using Microsoft.Extensions.Configuration;

using RabbitMQ.Wrapper.Models;

using System;

namespace Ponger
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WindowWidth = 50;
            Console.WriteLine("Ponger started!");

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            using var eventBus = new EventBusRabbitMq(
                NameConstants.PingPongExchange,
                NameConstants.PongQueue,
                NameConstants.PongKey,
                NameConstants.PingKey,
                new DefaultConnectionRabbitMq(new RabbitMqConfiguration(config)));

            eventBus.ListenQueue();
        }
    }
}